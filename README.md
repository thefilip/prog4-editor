# Proj23-M3 Grafický editor

## Filip Sršeň

Projekt sa zaoberá aplikáciuou na tvorbu obrázkov.

### Funkcie aplikácie:

+ výber veľkosti štetca
+ výber farby štetca
+ výber funkcie štetca
+ vrstvy
+ exportovanie do PNG formátu
+ ukladanie projektov do interného formátu

### Funkcie štetca:

+ ceruzka
+ štetec
+ sprej
+ kýblik
+ guma

## Ovládanie

Menu `File` ponúka funkcie `New` - to zmaže aktuálnu kresbu a vytvorí prázdne plátno.
`Save project` - uloží projekt do používateľom vybratého súboru.
`Open project` - načíta projekt z používateľom vybratého súboru.
`Export` - uloží obrázok do používateľom vybratého súboru.
`Exit` - vypne aplikáciu.

Veľkosť štetca sa vyberá na slidery. Farba sa vyberá vo vyberačí farby. Pomocou `New Layer` vytvoríme novú vrstvu.
Medzi vrstvami si vieme vyberať v drop-down menu. Funckie štetca vyberieme v jednom z piatich radio buttonov. Jeho tvar
v jednom z nasledujúcich radiu buttonov.
