import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;
import java.io.*;
import java.util.List;

/**
 * EditorController class is used for controlling JavaFX GUI components.
 * It connects onto FXML file describing the components.
 */
public class EditorController {


    @FXML
    public RadioButton fillRadioButton;
    @FXML
    public ComboBox<String> layerBox;
    @FXML
    public Button addLayer;
    public RadioButton brushRadioButton;
    public RadioButton pencilRadioButton;


    @FXML
    private Canvas canvas;

    @FXML
    private ColorPicker colorPicker;

    @FXML
    private Label brushSize;

    @FXML
    private Slider brushSlider;

    @FXML
    private RadioButton squareRadioButton;

    @FXML
    private RadioButton circleRadioButton;

    @FXML
    private RadioButton sprayRadioButton;
    @FXML
    public RadioButton eraserRadioButton;

    @FXML
    private ToggleGroup shapeToggleGroup;

    @FXML
    private ToggleGroup brushToggleGroup;

    private Painter p;

    private CanvasState state;

    private double bSize = 20;

    private int currentLayer = 0;

    /// 0 - square, 1 - circle, 2 - spray, 3 - eraser
    private int brushType = 10;
    private int brushShape = 0;

    /**
     * Initializes graphics components and some of their simpler onAction functions.
     */
    public void initialize() {
        state = new CanvasState(canvas);
        p = new Painter(state);

        brushSize.setText(String.valueOf(bSize));
        brushSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            bSize = newValue.doubleValue();
            brushSize.setText(String.format("%.0f", bSize));
        });

        brushToggleGroup = new ToggleGroup();
        pencilRadioButton.setToggleGroup(brushToggleGroup);
        brushRadioButton.setToggleGroup(brushToggleGroup);
        sprayRadioButton.setToggleGroup(brushToggleGroup);
        fillRadioButton.setToggleGroup(brushToggleGroup);
        eraserRadioButton.setToggleGroup(brushToggleGroup);

        shapeToggleGroup = new ToggleGroup();
        squareRadioButton.setToggleGroup(shapeToggleGroup);
        circleRadioButton.setToggleGroup(shapeToggleGroup);

        brushToggleGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {

            if (newValue == pencilRadioButton) {
                brushType = 10;
            } else if (newValue == brushRadioButton) {
                brushType = 20;
            } else if (newValue == sprayRadioButton) {
                brushType = 30;
            } else if (newValue == fillRadioButton) {
                brushType = 40;
            } else if (newValue == eraserRadioButton) {
                brushType = 100;
            }
        });


        shapeToggleGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {

            if (newValue == squareRadioButton) {
                brushShape = 0;
            } else if (newValue == circleRadioButton) {
                brushShape = 1;
            }
        });


        canvas.setOnMouseDragged(e -> {
            paint(e.getX(), e.getY());

        });
        canvas.setOnMouseClicked(e -> {
            paint(e.getX(), e.getY());
        });

        layerBox.setOnAction(e -> {
            String val = layerBox.getValue();
            System.out.println(val);
            if (val != null) currentLayer = Integer.parseInt(val.split(" ")[1]) - 1;
            System.out.println(currentLayer);
        });
    }

    /**
     * Paints on the canvas at mouse position depending on selected size and brushType.
     * Also uses private class variables brushType + brushShape.
     * <p>
     * Adds them to produce 2-digit numbers (3-digit for erasing).
     * <p>
     * First digit - 1 for brush 2 for pencil 3 for spray 4 for fill
     * <p>
     * Second digit - 0 for square shape 1 for circle shape
     *
     * @param eX x value of mouse position
     * @param eY y value of mouse position
     */
    public void paint(double eX, double eY) {
        System.out.println(brushShape + brushType);
        double size = bSize;
        double x = eX;
        double y = eY;
        switch (brushType + brushShape) {
            case 10 -> p.paintSquareAreaWithPencil(x, y, size, colorPicker.getValue(), currentLayer);
            case 11 -> p.paintCircularAreaWithPencil(x, y, size, colorPicker.getValue(), currentLayer);
            case 20 -> p.paintSquareAreaWithBrush(x, y, size, colorPicker.getValue(), currentLayer);
            case 21 -> p.paintCircularAreaWithBrush(x, y, size, colorPicker.getValue(), currentLayer);

            case 30 -> p.paintSquareAreaRandomly(x, y, size, 0.03, colorPicker.getValue(), currentLayer);
            case 31 -> p.paintCircularAreaRandomly(x, y, size, 0.03, colorPicker.getValue(), currentLayer);
            case 40, 41 ->
                    p.bucketFill(x, y, currentLayer, state.layerList.get(currentLayer)[(int) eX][(int) eY], colorPicker.getValue());
            case 100 -> p.eraseSquareArea(x, y, size, currentLayer);
            case 101 -> p.eraseCircularArea(x, y, size, currentLayer);
        }
    }

    /**
     * Handles exporting to PNG file.
     * Opens FileChooser to let user choose the file name and location.
     */
    public void onExport() {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Export File");
            fileChooser.setInitialFileName("image.png");
            fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("PNG file", "*.png"));
            File file = fileChooser.showSaveDialog(null);
            Image snapshot = canvas.snapshot(null, null);
            ImageIO.write(SwingFXUtils.fromFXImage(snapshot, null), "png", file);
        } catch (IOException e) {
            System.out.println("Failed to save");
        }
    }

    /**
     * Resets the CanvasState object.
     */
    public void onNew(ActionEvent actionEvent) {
        state = new CanvasState(canvas);
        p = new Painter(state);

    }

    /**
     * Exits the application.
     */
    public void onExit() {
        System.exit(0);
    }

    /**
     * Serializes the savable object return from {@link CanvasState#getSavableObject()} and saves it to `object.ser`.
     */
    public void onSave() {
        long startTime = System.currentTimeMillis();
        try {
            System.out.println("Trying to save");
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialFileName("project.proj");
            fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Project file", "*.proj"));
            File file = fileChooser.showSaveDialog(null);
            FileOutputStream fileOut = new FileOutputStream(file);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            List<SerializableColor[][]> f = state.getSavableObject();
            out.writeObject(f);
            out.close();
            fileOut.close();
            System.out.println("Object serialized and exported successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        long endTime = System.currentTimeMillis();
        long executionTime = endTime - startTime;
        System.out.println("Function execution time: " + executionTime + " milliseconds");
    }

    /**
     * Reads serialized object and loads it with {@link CanvasState#loadSavableObject(List)} objects
     * and repaints the canvas.
     */
    public void onOpen() {
        long startTime = System.currentTimeMillis();

        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Project file", "*.proj"));
            File file = fileChooser.showOpenDialog(null);
            FileInputStream fileIn = new FileInputStream(file);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            state.loadSavableObject((List<SerializableColor[][]>) in.readObject());
            in.close();
            fileIn.close();
            addNewLayerOption();
            System.out.println("Object loaded and deserialized successfully.");
            // Use the deserialized object as needed
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        long endTime = System.currentTimeMillis();
        long executionTime = endTime - startTime;
        System.out.println("Loading function execution time: " + executionTime + " milliseconds");
    }

    /**
     * Creates new layer that is of {@link Color#TRANSPARENT}.
     */
    public void addNewLayer() {
        System.out.println("creating new layer");
        state.createLayer(Color.TRANSPARENT);
        addNewLayerOption();
    }

    /**
     * Adds all the layers into the {@link ComboBox} with {@link ComboBox#setItems(ObservableList)} and sets the value ({@link ComboBox#setValue(Object)}) for the current layer.
     */

    public void addNewLayerOption() {
        System.out.println("creating new layer");
        ObservableList<String> data = FXCollections.observableArrayList();
        for (int i = 1; i <= state.layerList.size(); i++) {
            data.add("Layer " + i);
        }
        layerBox.setItems(data);
        layerBox.setValue(data.get(currentLayer));
    }

}
