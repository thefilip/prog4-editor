import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Application runner class
 */
public class EditorApp extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        stage.setScene(new Scene(FXMLLoader.load(getClass().getResource("editor.fxml"))));
        stage.setTitle("Editor");
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
