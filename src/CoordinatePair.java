import javafx.scene.paint.Color;

/**
 * Polymorph class used to create a pair of coordinates in a single object. Used in {@link Painter#bucketFill} DFS algorithm.
 *
 * @param <E> desired type
 */
public class CoordinatePair<E> {
    private E x;
    private E y;

    /**
     * @param x x positional coordinate
     * @param y y positional coordinate
     */
    public CoordinatePair(E x, E y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Get x positional coordinate
     *
     * @return x value
     */

    public E getX() {
        return x;
    }

    /**
     * Get y positional coordinate
     *
     * @return y value
     */
    public E getY() {
        return y;
    }

    /**
     * String of coordinate pair.
     *
     * @return String value of the pair
     */

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj
     */

    @Override
    public boolean equals(Object obj) {
        CoordinatePair a = (CoordinatePair) obj;
        return (a.getX().equals(this.getX()) && a.getY().equals(this.getY()));
    }

    /**
     * Returns a hash code value for the object. This method is
     * supported for the benefit of hash tables such as those provided by
     * {@link java.util.HashMap}.
     *
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        return (int) x * 1200 + (int) y;
    }
}