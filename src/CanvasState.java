import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * A class that manages the canvas, layers, keeps its state and manages its saving and loading.
 */
public class CanvasState {
    /**
     * List of layers. List of 2D arrays width * height of {@link Color} - representing each pixel of each layer.
     */
    public List<Color[][]> layerList = new ArrayList<>();
    private GraphicsContext g;
    /**
     * Width of {@link Canvas}.
     */
    public int width;
    /**
     * Height of {@link Canvas}.
     */

    public int height;

    /**
     * Creates first empty(white) layer.
     *
     * @param c {@link Canvas} component from the screen.
     */
    public CanvasState(Canvas c) {
        this.g = c.getGraphicsContext2D();
        width = (int) c.getWidth();
        height = (int) c.getHeight();
        createLayer(Color.WHITE);
        System.out.println(layerList.size());
    }

    public Color getColorAt(int x, int y, int layer) {
        return layerList.get(layer)[x][y];
    }

    /**
     * Paints pixel on set coordinates and saves it color to a data structure.
     *
     * @param x     x coordinate the of pixel
     * @param y     y coordinate the of pixel
     * @param layer layer of the pixel
     * @param color color to be painted
     */
    public void paintPixel(int x, int y, int layer, Color color) {
        layerList.get(layer)[x][y] = color;
        g.setFill(color);
        g.fillRect(x, y, 1, 1);
    }

    /**
     * Repaints pixel to its original value, as saved in the data structure. Used for loading older projects.
     *
     * @param x     x coordinate the of pixel
     * @param y     y coordinate the of pixel
     * @param layer layer of the pixel
     */
    public void repaintPixel(int x, int y, int layer) {
        g.setFill(layerList.get(layer)[x][y]);
        g.fillRect(x, y, 1, 1);
    }

    /**
     * Creates a new layer with selected color.
     *
     * @param c selected color
     */
    public void createLayer(Color c) {
        Color[][] pix = new Color[width][height];
        layerList.add(pix);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                layerList.get(layerList.size() - 1)[i][j] = c;
                paintPixel(i, j, layerList.size() - 1, c);
            }
        }

    }

    /**
     * Converts the List of Color[][] data structure to List of SerializableColor[][] to enable serialization.
     *
     * @return List SerializableColor[][] of 2D arrays each representing a layer.
     */
    public List<SerializableColor[][]> getSavableObject() {
        List<SerializableColor[][]> out = new ArrayList<>();
        System.out.println("Starting saving object ");
        for (int i = 0; i < layerList.size(); i++) {
            System.out.println("Starting layer " + i);
            SerializableColor[][] layer = new SerializableColor[width][height];
            for (int w = 0; w < width; w++) {
                for (int h = 0; h < height; h++) {
                    layer[w][h] = new SerializableColor(layerList.get(i)[w][h]);
                }
            }
            out.add(layer);
        }
        return out;
    }

    /**
     * Repaints all pixel in layer to their original value, as saved in the data structure. Used for loading older projects.
     */
    public void repaintLayers(List<Color[][]> old) {
        System.out.println("repainting canvas");
        for (int i = 0; i < layerList.size(); i++) {
            Color[][] layer = layerList.get(i);
            for (int w = 0; w < width; w++) {
                for (int h = 0; h < height; h++) {
                    if (i < old.size()) {
                        if (!layer[w][h].equals(old.get(i)[w][h])) {
                            g.setFill(layer[w][h]);
                            g.fillRect(w, h, 1, 1);
                        }
                    } else {
                        g.setFill(layer[w][h]);
                        g.fillRect(w, h, 1, 1);
                    }
                }
            }
        }
    }

    /**
     * Converts from List of SerializableColor[][] to List of Color[][]. Resets all current data in process.
     *
     * @param in List of 2D arrays each representing a layer.
     */
    public void loadSavableObject(List<SerializableColor[][]> in) {
        long startTime = System.currentTimeMillis();
        System.out.println("loading object");
        List<Color[][]> aa = layerList;
        layerList = new ArrayList<>();
        for (int i = 0; i < in.size(); i++) {
            Color[][] layer = new Color[width][height];
            for (int w = 0; w < width; w++) {
                for (int h = 0; h < height; h++) {
                    layer[w][h] = in.get(i)[w][h].getColor();
                }
            }
            layerList.add(layer);
        }

        long endTime = System.currentTimeMillis();
        long executionTime = endTime - startTime;
        System.out.println("Loading time: " + (executionTime / 1000.0) + "seconds");
        repaintLayers(aa);
    }


}
