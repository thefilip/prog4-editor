import java.io.Serializable;

import javafx.scene.paint.Color;

/**
 * Used for being able to serialize color. Breaks 4 RGBA values into 4 double variables.
 */
public class SerializableColor implements Serializable {
    final double r;
    final double g;
    final double b;
    final double a;

    public SerializableColor(Color color) {
        r = color.getRed();
        g = color.getGreen();
        b = color.getBlue();
        a = color.getOpacity();
    }

    public Color getColor() {
        return new Color(r, g, b, a);
    }
}