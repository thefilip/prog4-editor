import javafx.scene.paint.Color;

import java.util.*;

/**
 * Class that manages painting to the canvas with various brushes.
 */

public class Painter {
    private CanvasState cs;

    private Random rnd = new Random(314159L);

    /**
     * Sets class variable {@link CanvasState}.
     *
     * @param c {@link CanvasState} used for accessing the canvas and painting individual pixels.
     */

    public Painter(CanvasState c) {
        this.cs = c;
    }

    /**
     * Paints a square area (size*size) around centerX and centerY. With brush - the further from center the lower opacity.
     *
     * @param centerX x coordinate of center of square area
     * @param centerY y coordinate of center of square area
     * @param size    size to be filled
     * @param color   color to use for painting
     * @param layer   which layer to put pixels onto
     */

    public void paintSquareAreaWithBrush(double centerX, double centerY, double size, Color color, int layer) {
        int startX = (int) (centerX - size / 2);
        int endX = (int) (centerX + size / 2);
        int startY = (int) (centerY - size / 2);
        int endY = (int) (centerY + size / 2);
        for (int i = startX; i < endX; i++) {
            for (int j = startY; j < endY; j++) {
                double distance = Math.sqrt(Math.pow(centerX - i, 2) + Math.pow(centerY - j, 2));
                double opacity = 1 - (distance / size);
                if (i >= 0 && j >= 0 && i < cs.width && j < cs.height && !color.equals(cs.getColorAt(i, j, layer)))
                    cs.paintPixel(i, j, layer, new Color(color.getRed(), color.getGreen(), color.getBlue(), opacity));
            }

        }
    }

    /**
     * Paints a square area (size*size) around centerX and centerY. With pencil - just fill the whole area.
     *
     * @param centerX x coordinate of center of square area
     * @param centerY y coordinate of center of square area
     * @param size    size to be filled
     * @param color   color to use for painting
     * @param layer   which layer to put pixels onto
     */
    public void paintSquareAreaWithPencil(double centerX, double centerY, double size, Color color, int layer) {
        int startX = (int) (centerX - size / 2);
        int endX = (int) (centerX + size / 2);
        int startY = (int) (centerY - size / 2);
        int endY = (int) (centerY + size / 2);
        for (int i = startX; i < endX; i++) {
            for (int j = startY; j < endY; j++) {
                if (i >= 0 && j >= 0 && i < cs.width && j < cs.height && !color.equals(cs.getColorAt(i, j, layer)))
                    cs.paintPixel(i, j, layer, color);
            }

        }
    }

    /**
     * Paints a circular area around centerX, centerY with diameter of size. With brush - the further from center the lower opacity.
     *
     * @param centerX x coordinate of center of circular area
     * @param centerY y coordinate of center of circular area
     * @param size    size to be filled
     * @param color   color to use for painting
     * @param layer   which layer to put pixels onto
     */

    public void paintCircularAreaWithBrush(double centerX, double centerY, double size, Color color, int layer) {
        int startX = (int) (centerX - (size / 2));
        int endX = (int) (centerX + (size / 2));
        int startY = (int) (centerY - (size / 2));
        int endY = (int) (centerY + (size / 2));
        double r = size / 2;
        for (int i = startX; i < endX; i++) {
            for (int j = startY; j < endY; j++) {
                double distance = Math.sqrt(Math.pow(centerX - i, 2) + Math.pow(centerY - j, 2));
                if (i >= 0 && j >= 0 && i < cs.width && j < cs.height && distance < r && !color.equals(cs.getColorAt(i, j, layer))) {
                    double opacity = 1 - (distance / r);
                    cs.paintPixel(i, j, layer, new Color(color.getRed(), color.getGreen(), color.getBlue(), opacity));
                }
            }

        }
    }

    /**
     * Paints a circular area around centerX, centerY with diameter of size. With pencil - just fill the whole area.
     *
     * @param centerX x coordinate of center of circular area
     * @param centerY y coordinate of center of circular area
     * @param size    size to be filled
     * @param color   color to use for painting
     * @param layer   which layer to put pixels onto
     */

    public void paintCircularAreaWithPencil(double centerX, double centerY, double size, Color color, int layer) {
        int startX = (int) (centerX - (size / 2));
        int endX = (int) (centerX + (size / 2));
        int startY = (int) (centerY - (size / 2));
        int endY = (int) (centerY + (size / 2));
        double r = size / 2;
        for (int i = startX; i < endX; i++) {
            for (int j = startY; j < endY; j++) {
                double distance = Math.sqrt(Math.pow(centerX - i, 2) + Math.pow(centerY - j, 2));
                if (i >= 0 && j >= 0 && i < cs.width && j < cs.height && distance < r && !color.equals(cs.getColorAt(i, j, layer))) {
                    cs.paintPixel(i, j, layer, color);
                }
            }

        }
    }


    /**
     * Paints a square area (size*size) around centerX and centerY. It fills individual pixel randomly.
     *
     * @param centerX x coordinate of center of square area
     * @param centerY y coordinate of center of square area
     * @param size    size to be filled
     * @param chance  (0 - 1.0f) what is the chance of painting a certain pixel.
     * @param color   color to use for painting
     * @param layer   which layer to put pixels onto
     */

    public void paintSquareAreaRandomly(double centerX, double centerY, double size, double chance, Color color, int layer) {
        int startX = (int) (centerX - size / 2);
        int endX = (int) (centerX + size / 2);
        int startY = (int) (centerY - size / 2);
        int endY = (int) (centerY + size / 2);
        for (int i = startX; i < endX; i++) {
            for (int j = startY; j < endY; j++) {
                if (i >= 0 && j >= 0 && i < cs.width && j < cs.height && rnd.nextFloat(1) < chance && !color.equals(cs.getColorAt(i, j, layer)))
                    cs.paintPixel(i, j, layer, color);
            }


        }
    }

    /**
     * Paints a circular area around centerX, centerY with diameter of size. It fills individual pixel randomly.
     *
     * @param centerX x coordinate of center of square area
     * @param centerY y coordinate of center of square area
     * @param size    size to be filled
     * @param chance  (0 - 1.0f) what is the chance of painting a certain pixel.
     * @param color   color to use for painting
     * @param layer   which layer to put pixels onto
     */

    public void paintCircularAreaRandomly(double centerX, double centerY, double size, double chance, Color color, int layer) {
        int startX = (int) (centerX - size / 2);
        int endX = (int) (centerX + size / 2);
        int startY = (int) (centerY - size / 2);
        int endY = (int) (centerY + size / 2);
        double r = size / 2;
        for (int i = startX; i < endX; i++) {
            for (int j = startY; j < endY; j++) {
                double distance = Math.sqrt(Math.pow(centerX - i, 2) + Math.pow(centerY - j, 2));
                if (i >= 0 && j >= 0 && i < cs.width && j < cs.height && rnd.nextFloat(1) < chance && !color.equals(cs.getColorAt(i, j, layer))
                        && distance < r)
                    cs.paintPixel(i, j, layer, color);
            }


        }
    }

    /**
     * Erases a square area (size*size) around centerX and centerY.
     *
     * @param centerX x coordinate of center of square area
     * @param centerY y coordinate of center of square area
     * @param size    size to be filled
     * @param layer   which layer to put pixels onto
     */

    public void eraseSquareArea(double centerX, double centerY, double size, int layer) {
        int startX = (int) (centerX - size / 2);
        int endX = (int) (centerX + size / 2);
        int startY = (int) (centerY - size / 2);
        int endY = (int) (centerY + size / 2);
        for (int i = startX; i < endX; i++) {
            for (int j = startY; j < endY; j++) {
                if (i >= 0 && j >= 0 && i < cs.width && j < cs.height) {
                    if (layer > 0) {
                        cs.layerList.get(layer)[i][j] = Color.TRANSPARENT;
                        cs.repaintPixel(i, j, layer - 1);
                    } else cs.paintPixel(i, j, layer, Color.WHITE);
                }
            }

        }
    }

    public void eraseCircularArea(double centerX, double centerY, double size, int layer) {
        int startX = (int) (centerX - size / 2);
        int endX = (int) (centerX + size / 2);
        int startY = (int) (centerY - size / 2);
        int endY = (int) (centerY + size / 2);
        double r = size / 2;
        for (int i = startX; i < endX; i++) {
            for (int j = startY; j < endY; j++) {
                double distance = Math.sqrt(Math.pow(centerX - i, 2) + Math.pow(centerY - j, 2));
                if (i >= 0 && j >= 0 && i < cs.width && j < cs.height && distance < r) {
                    if (layer > 0) {
                        cs.layerList.get(layer)[i][j] = Color.TRANSPARENT;
                        cs.repaintPixel(i, j, layer - 1);
                    } else cs.paintPixel(i, j, layer, Color.WHITE);
                }
            }

        }
    }

    /**
     * Fills an area bound by canvas edges and different colors.
     *
     * @param x             x coordinate of the staring pixel
     * @param y             y coordinate of the staring pixel
     * @param layer         which layer to put pixels onto
     * @param originalColor original color of the starting pixel
     * @param newColor      color to use for painting
     */
    public void bucketFill(double x, double y, int layer, Color originalColor, Color newColor) {
        Queue<CoordinatePair<Integer>> q = new ArrayDeque<>();
        q.add(new CoordinatePair<>((int) x, (int) y));
        Set<CoordinatePair<Integer>> visited = new HashSet<>();
        while (!q.isEmpty()) {
            CoordinatePair<Integer> node = q.remove();
            if (!visited.contains(node) && cs.layerList.get(layer)[node.getX()][node.getY()].equals(originalColor)) {
                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        int xx = node.getX() + i;
                        int yy = node.getY() + j;
                        if (xx >= 0 && yy >= 0 && xx < cs.width && yy < cs.height) q.add(new CoordinatePair<>(xx, yy));
                    }
                }
                cs.paintPixel(node.getX(), node.getY(), layer, newColor);
            }
            visited.add(node);
        }


    }
}
